# hugo-docker #

Base Docker image with [Hugo](https://gohugo.io) and `awscli`

# CI #
I use this for deploying my Hugo projects to aws s3

For sample gitlab ci config see my blog source code:

[https://gitlab.com/softinio/softinio.com](https://gitlab.com/softinio/softinio.com)

